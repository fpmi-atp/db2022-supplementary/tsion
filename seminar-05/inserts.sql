DROP SCHEMA IF EXISTS seminar_5;
CREATE SCHEMA seminar_5;

DROP TABLE IF EXISTS seminar_5.salary;
CREATE TABLE seminar_5.salary (
    name        VARCHAR(120),
    dt          DATE,
    salary_amt  DECIMAL(12, 2),
    salary_type SMALLINT
);

INSERT INTO seminar_5.salary VALUES ('Роздухова Нина', '2019-02-25', 2999.00, 1);
INSERT INTO seminar_5.salary VALUES ('Роздухова Нина', '2019-03-05', 5100.00, 1);
INSERT INTO seminar_5.salary VALUES ('Роздухова Нина', '2019-03-05', 6800.00, 3);
INSERT INTO seminar_5.salary VALUES ('Халяпов Александр', '2019-02-25', 10499.00, 1);
INSERT INTO seminar_5.salary VALUES ('Халяпов Александр', '2019-03-05', 13000.00, 1);
INSERT INTO seminar_5.salary VALUES ('Меркурьева Надежда', '2019-02-25', 2999.00, 1);
INSERT INTO seminar_5.salary VALUES ('Меркурьева Надежда', '2019-02-25', 5800.00, 2);
INSERT INTO seminar_5.salary VALUES ('Меркурьева Надежда', '2019-03-05', 6400.00, 1);
INSERT INTO seminar_5.salary VALUES ('Меркурьева Надежда', '2019-03-05', 8300.00, 2);